<?php 
    abstract class Product{
        protected $conn;
        //Product props
        public $id;
        public $sku;
        public $name;
        public $price;
        public $type;
        public $specs;
        //constructor
        public function __construct($db){
            $this->conn=$db;
        }
        //abstract functions to be implemented in the other classes
        abstract public function read();
        abstract public function create();
        abstract public function delete();
    }
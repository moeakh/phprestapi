<?php
    class AddAndDelete extends Product{
        public function read(){
            return "nothing to read";
        }
        public function create(){
            static $query = "INSERT INTO `product` 
                SET
                sku=:sku,
                name=:name,
                price=:price,
                type=:type,
                specs=:specs
            ";
            // prepare statment
            $stmt = $this->conn->prepare($query);
            // clean the data
            $this->sku=htmlspecialchars(strip_tags($this->sku));
            $this->name=htmlspecialchars(strip_tags($this->name));
            $this->price=htmlspecialchars(strip_tags($this->price));
            $this->type=htmlspecialchars(strip_tags($this->type));
            $this->specs=htmlspecialchars(strip_tags($this->specs));
            // Bind Params
            $stmt->bindParam(':sku', $this->sku);
            $stmt->bindParam(':name', $this->name);
            $stmt->bindParam(':price', $this->price);
            $stmt->bindParam(':type', $this->type);
            $stmt->bindParam(':specs', $this->specs);
            //excute the statment
            if($stmt->execute()){
                return true;
            }else{
                printf('Error: %s\n',$stmt->error);
                return false;
            }
        }
        public function delete(){
            $query= "DELETE FROM `product` WHERE `id`=:id";
            $stmt= $this->conn->prepare($query);
            
            $this->spidecs=htmlspecialchars(strip_tags($this->id));

            $stmt->bindParam(':id', $this->id);

            if($stmt->execute()){
                return true;
            }else{
                printf('Error: %s\n',$stmt->error);
                return false;
            }
        }
    }
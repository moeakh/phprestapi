<?php 
    class Book extends Product{
        public function read(){
            static $query = "SELECT `product`.`id`,
            `product`.`sku`,
            `product`.`name`,
            `product`.`price`,
            `product`.`type`,
            `product`.`specs`
        FROM `product` WHERE `product`.`type`='Book'";
            // prepare statment
            $stmt = $this->conn->prepare($query);
            //excute the statment
            $stmt->execute();
            return $stmt;
        }
        public function create(){
            return "Nothing to add";
        }
        public function delete(){
            return "Nothing to delete";
        }
    }
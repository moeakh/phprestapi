<?php
    include_once '../../config/Database.php';
    include_once '../../Models/Product.php';
    include_once '../../Models/Book.php';
    include_once '../../Models/DVD.php';
    include_once '../../Models/Furniture.php';
    include_once '../../Models/GeneralEndPoint.php';
    
    // Include CORS headers
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE');
	header('Access-Control-Allow-Headers: X-Requested-With');
	header('Content-Type: application/json');

	// create a api variable to get HTTP method dynamically
	$api = $_SERVER['REQUEST_METHOD'];
	// Get all or a single user from database
	if ($api == 'GET') {
        $product = new ProductApi();
	    $product->read();
	}
	if ($api == 'POST') {
        $product = new ProductApi();
	    $product->create();
	}
	if ($api == 'DELETE') {
        $product = new ProductApi();
	    $product->delete();
	}

    class ProductApi{
        protected $database;
        public function __construct(){
            $db = new Database();
            $this->database = $db->connect();
        }
    
        public function create(){
            //instantiate database
            $database = new Database();
            $db=$database->connect();
            //instantiate product object
            $product = new AddAndDelete($db);
            //get product data
            $data=json_decode(file_get_contents('php://input'));
            if($data->sku == ""||$data->name == ""||$data->price == ""||$data->type == ""||$data->specs == ""){
                echo json_encode(array('message'=>'Product not added'));
            }else{
            $product->sku = $data->sku;
            $product->name = $data->name;
            $product->price = $data->price;
            $product->type = $data->type;
            $product->specs = $data->specs;

            if($product->create()){
                echo json_encode(array('message'=>'Product added'));
            }else{
                echo json_encode(array('message'=>'Product not added'));
            }}
        }
        public function read(){
            $product = new Book($this->database);
            $result = $product->read();
            $num = $result->rowCount();
            if ($num > 0) {
                $product_array= array();
                $product_array['data'] = array();
                while($row = $result->fetch(PDO::FETCH_ASSOC)){
                    extract($row);
                    $product_item = array(
                        'id' => $id,
                        'sku' => $sku,
                        'name' => $name,
                        'price' => $price,
                        'type' => $type,
                        'specs' => $specs
                    );
                    array_push($product_array['data'], $product_item);
                }}
            $product = new DVD($this->database);
            $result = $product->read();
            $num = $result->rowCount();
            if ($num > 0) {
                while($row = $result->fetch(PDO::FETCH_ASSOC)){
                    extract($row);
                    $product_item = array(
                        'id' => $id,
                        'sku' => $sku,
                        'name' => $name,
                        'price' => $price,
                        'type' => $type,
                        'specs' => $specs
                    );
                    array_push($product_array['data'], $product_item);
                }}
            $product = new Furniture($this->database);
            $result = $product->read();
            $num = $result->rowCount();
            if ($num > 0) {
                while($row = $result->fetch(PDO::FETCH_ASSOC)){
                    extract($row);
                    $product_item = array(
                        'id' => $id,
                        'sku' => $sku,
                        'name' => $name,
                        'price' => $price,
                        'type' => $type,
                        'specs' => $specs
                    );
                    array_push($product_array['data'], $product_item);
                }}
            echo json_encode($product_array);
        }
        public function delete()
        {
            //instantiate database
            $database = new Database();
            $db=$database->connect();
            //instantiate product object
            $product = new AddAndDelete($db);
            //get product data
            $data=json_decode(file_get_contents('php://input'));

            $product->id = $data->id;

            if($product->delete()){
                echo json_encode(array('message'=>'Product deleted'));
            }else{
                echo json_encode(array('message'=>'Product dont exist'));
            }
        }
    }